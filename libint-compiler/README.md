# libint2 2.7.0 compiler

To compile the compiler from the source code, the following command can be used:

```sh
CXX=icpc CC=icc CPPFLAGS='-I/path/to/boost_x_yy_z/boost' /path/to/libint/configure \
    --enable-1body=2 \
    --enable-eri=2 \
    --enable-eri3=2 \
    --enable-eri2=2 \
    --with-max-am=5 \
    --with-opt-am=4 \
    --with-eri-max-am=5,4,3 \
    --with-eri-opt-am=4,3,2 \
    --enable-generic-code \
    --enable-contracted-ints \
    --with-cxx-optflags='-O3' \
    --with-cxxgen-optflags='-O3' \
    --enable-shared \
    --with-build-id='cbh31-250821' \
    --enable-mpfr=/path/to/mpfr-x.y.z-compiled/lib \
    --with-boost=/path/to/boost_x_yy_z/boost \
    --prefix=/path/to/out/of/source/prefix
```
